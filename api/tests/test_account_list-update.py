from fastapi.testclient import TestClient
from main import app
from queries.accounts import AdminRepository, AdminOutWithPassword
from authenticator import authenticator

client = TestClient(app)


class EmptyAccountRepository:
    def get_all(self):
        return [
            {
                'id': 1,
                'username': 'test1',
                'hashed_password': 'mypassword'
            },
            {
                'id': 2,
                'username': 'test2',
                'hashed_password': 'yourpassword'
            },
            {
                'id': 3,
                'username': 'test3',
                'hashed_password': 'herpassword'
            }
        ]


class UpdateAccountRepository:
    def update(self, admin: dict, hashed_password: str):
        return AdminOutWithPassword(
            id=admin["id"],
            username=admin["username"],
            hashed_password=hashed_password,
        )


# Dummy Authentication Information vvv
def fake_get_current_account_data():
    return {
        'id': 1,
        'username': 'string',
        'hashed_password':
        '$2b$12$jhfP5TLFItrE5Ar9lTWtxOyDjADHFvXxJ6sm5LkjQRzO/VUf6Yl1a'
        }


def test_get_all_accounts():
    # Arrange
    app.dependency_overrides[AdminRepository] = EmptyAccountRepository
    app.dependency_overrides[
        authenticator.try_get_current_account_data
        ] = fake_get_current_account_data
    # Act
    response = client.get("/admin")
    app.dependency_overrides = {}
    # Assert ()
    assert response.status_code == 200
    assert response.json() == [
            {
                'id': 1,
                'username': 'test1',
            },
            {
                'id': 2,
                'username': 'test2',
            },
            {
                'id': 3,
                'username': 'test3',
            },
        ]


def test_account_update():
    app.dependency_overrides[AdminRepository] = UpdateAccountRepository
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data

    response = client.put(
        "/admin/update",
        json={
            'id': 1,
            'username': 'string',
            'new_password': 'IAmAwesome',
        }
    )
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
        'id': 1,
        'username': 'string'
        }
