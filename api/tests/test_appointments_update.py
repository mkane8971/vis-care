from fastapi.testclient import TestClient
from main import app
from queries.appointments import AppointmentRepository, AppointmentOut
from authenticator import authenticator
# from datetime import datetime
from queries.accounts import AdminOutWithPassword

client = TestClient(app)


class MockAppointmentRepository:
    def update(self, appointment_id: int, appointment):
        return AppointmentOut(
            id=appointment_id,
            visitor_name=appointment.visitor_name,
            group_size=appointment.group_size,
            date_time=appointment.date_time,
            location=appointment.location,
            phone_number=appointment.phone_number,
            email=appointment.email
        )


def fake_get_current_account_data():
    return AdminOutWithPassword(
        id=1,
        username="string",
        hashed_password="string"
    )


def test_update_appointment():
    app.dependency_overrides[AppointmentRepository] = MockAppointmentRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data

    test_appointment = {
        "visitor_name": "Test Visitor",
        "group_size": 4,
        "date_time": "2024-02-01T02:17:35.069000+00:00",
        "location": 1,
        "phone_number": "123-333-3333",
        "email": "unit@test.com"
    }

    response = client.put("/appointments/1", json=test_appointment)
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "visitor_name": "Test Visitor",
        "group_size": 4,
        "date_time": "2024-02-01T02:17:35.069000+00:00",
        "location": 1,
        "phone_number": "123-333-3333",
        "email": "unit@test.com"
    }
