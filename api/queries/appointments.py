from pydantic import BaseModel
from queries.pool import pool
from datetime import datetime
from typing import List, Optional


class Appointment(BaseModel):
    visitor_name: str
    group_size: int
    date_time: datetime
    location: int
    phone_number: str
    email: str


class AppointmentOut(BaseModel):
    id: int
    visitor_name: str
    group_size: int
    date_time: datetime
    location: int
    phone_number: str
    email: str


class AppointmentRepository:
    def create(self, appointment: Appointment) -> AppointmentOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO appointments
                        (visitor_name,
                        group_size,
                        date_time,
                        location,
                        phone_number,
                        email)
                    VALUES
                        (%s, %s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [appointment.visitor_name,
                     appointment.group_size,
                     appointment.date_time,
                     appointment.location,
                     appointment.phone_number,
                     appointment.email]
                )
                id = result.fetchone()[0]
                return AppointmentOut(id=id, **appointment.dict())

    def get_all(self) -> List[AppointmentOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id,
                    visitor_name,
                    group_size,
                    date_time,
                    location,
                    phone_number,
                    email
                    FROM appointments;
                    """
                )
                result = []
                for record in db.fetchall():
                    appointment = AppointmentOut(
                        id=record[0],
                        visitor_name=record[1],
                        group_size=record[2],
                        date_time=record[3],
                        location=record[4],
                        phone_number=record[5],
                        email=record[6],
                    )
                    result.append(appointment)
                return result

    def get_one(self, appointment_id: int) -> Optional[AppointmentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM appointments
                        WHERE id = %s
                        """,
                        [appointment_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return AppointmentOut(
                        id=record[0],
                        visitor_name=record[1],
                        group_size=record[2],
                        date_time=record[3],
                        location=record[4],
                        phone_number=record[5],
                        email=record[6],
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not get that location"}

    def update(self,
               appointment_id: int,
               appointment: Appointment
               ) -> AppointmentOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE appointments
                    SET visitor_name = %s, group_size = %s, date_time = %s,
                        location = %s, phone_number = %s, email = %s
                        WHERE id = %s
                        RETURNING *;
                        """,
                    [
                        appointment.visitor_name,
                        appointment.group_size,
                        appointment.date_time,
                        appointment.location,
                        appointment.phone_number,
                        appointment.email,
                        appointment_id
                    ]
                )
                updated_record = db.fetchone()
                if updated_record:
                    return AppointmentOut(id=updated_record[0],
                                          visitor_name=updated_record[1],
                                          group_size=updated_record[2],
                                          date_time=updated_record[3],
                                          location=updated_record[4],
                                          phone_number=updated_record[5],
                                          email=updated_record[6])
                else:
                    raise ValueError("Appointment not found")

    def delete(self, appointment_id: int):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM appointments
                    WHERE id = %s
                    RETURNING id;
                    """,
                    [appointment_id]
                )
                deleted_id = db.fetchone()
                if deleted_id:
                    return {"message": "Appointment successfully deleted."}
                else:
                    raise ValueError("Appointment not found")
