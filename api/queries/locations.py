from pydantic import BaseModel
from queries.pool import pool
from typing import Optional, Union, List
# this file has nothing to do with the database. these are for our ENDPOINTS


class Error(BaseModel):
    message: str


class LocationIn(BaseModel):
    name: str


class LocationUpdate(LocationIn):
    picture_url: str


class LocationOut(BaseModel):
    id: int
    name: str
    picture_url: Optional[str] = None
    num_appts: Optional[int] = None


class LocationRepository(BaseModel):
    def create(
            self,
            location: LocationIn,
            picture_url: Optional[str] = None
    ) -> Union[LocationOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO locations
                            (name, picture_url)
                        VALUES
                            (%s, %s)
                        RETURNING id, name;
                        """,
                        (location.name, picture_url)
                    )
                    id, name = result.fetchone()
                    return LocationOut(
                        id=id,
                        name=name,
                        picture_url=picture_url
                    )
        except Exception as e:
            print(e)
            return Error(message="Could not Create Location")

    def get_one(self, location_id: int) -> Optional[LocationOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , name
                            , picture_url
                        FROM locations
                        WHERE id = %s
                        """,
                        [location_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return LocationOut(
                            id=record[0],
                            name=record[1],
                            picture_url=record[2],
                        )
        except Exception as e:
            print(e)
            return {"message": "Could not get that location"}

    def get_all(self) -> Union[Error, List[LocationOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT l.id
                            , l.name
                            , l.picture_url
                            , COUNT(a.id) AS num_appts
                        FROM locations l
                            LEFT JOIN appointments a ON (l.id=a.location)
                        GROUP BY l.id
                        ORDER BY name;
                        """
                    )
                    return [
                        LocationOut(
                            id=record[0],
                            name=record[1],
                            picture_url=record[2],
                            num_appts=record[3]
                        )
                        for record in db
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all locations from queries"}

    def update_location(
            self,
            id: int,
            location: LocationUpdate
    ) -> Union[LocationOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE locations
                        SET name = %s, picture_url = %s
                        WHERE id =%s
                        RETURNING *
                        """,
                        [location.name, location.picture_url, id]
                    )

                    old_data = location.dict()
                    if db.fetchone():
                        return LocationOut(id=id, **old_data)
                    return {"message": "could not update location"}
        except Exception:
            return {"message": "could not update location"}

    def delete(self, location_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM locations
                        WHERE id = %s
                        """,
                        [location_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def location_in_to_out(self, id: int, location: LocationIn):
        old_data = location.dict()
        return LocationOut(id=id, **old_data)

    def record_to_location_out(self, record):
        return LocationOut(
            id=record[0],
            name=record[1],
            picture_url=record[2],
        )
