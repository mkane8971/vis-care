from pydantic import BaseModel
from queries.pool import pool
from typing import Union, List, Optional

# this file has nothing to do with the database. these are for our ENDPOINTS


class DuplicateAccountError(ValueError):
    message: str


class Error(BaseModel):
    message: str


class AdminIn(BaseModel):
    username: str
    password: str


class AdminUpdate(BaseModel):
    new_password: str


class AdminOut(BaseModel):
    id: int
    username: str


class AdminOutWithPassword(AdminOut):
    hashed_password: str


class AdminRepository:
    def create(self, admin: AdminIn,
               hashed_password: str
               ) -> AdminOutWithPassword:
        # Below comes from psycopg documentation
        # connect to database
        try:
            with pool.connection() as conn:
                # get cursor (something to run SQL with)
                with conn.cursor() as db:
                    # run INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO admin
                            (username, hashed_password)
                        VALUES
                            (%s,%s)
                        RETURNING id,
                        username,
                        hashed_password;
                        """,
                        [admin.username, hashed_password]
                    )
                    id = result.fetchone()[0]
                    # return new data
                    old_data = admin.dict()
                    return AdminOutWithPassword(
                        id=id,
                        hashed_password=hashed_password,
                        **old_data)
        except Exception:
            return {"message": "Could not create admin"}

    def get(self, username: str) -> Optional[AdminOutWithPassword]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id,
                            username,
                            hashed_password
                        FROM admin
                        WHERE username = %s
                        """,
                        [username]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_admin_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that admin"}

    def get_all(self) -> Union[Error, List[AdminOut]]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # run our SELECT statement
                    db.execute(
                        """
                        SELECT id, username, hashed_password
                        FROM admin
                        ORDER BY id;
                        """
                    )
                    return [
                        AdminOut(
                            id=record[0],
                            username=record[1]
                        )
                        for record in db
                    ]
        except Exception as e:
            print(e)
            return {"message": "Unable to retrieve admin list."}

    def update(
            self,
            admin: dict,
            hashed_password: str
            ) -> Union[Error, List[AdminOutWithPassword]]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE admin
                        SET hashed_password = %s
                        WHERE id = %s
                        RETURNING id, username;
                        """,
                        [
                            hashed_password,
                            admin["id"]
                        ]
                    )
                    return self.admin_in_to_out(admin)
        except Exception as e:
            print(e)
            return {"message": "Unable to update information."}

    def admin_in_to_out(self, admin: dict):
        return AdminOut(id=admin["id"], username=admin["username"])

    def delete(self, admin_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM admin
                        WHERE id = %s
                        """,
                        [admin_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def record_to_admin_out(self, record) -> AdminOutWithPassword:
        admin_dict = {
            "id": record[0],
            "username": record[1],
            "hashed_password": record[2]
        }
        return admin_dict
