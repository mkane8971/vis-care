from fastapi import (
    APIRouter,
    Depends,
    Response,
)
from typing import Union, List, Optional
from authenticator import authenticator
from queries.locations import (
    Error,
    LocationIn,
    LocationRepository,
    LocationOut,
    LocationUpdate,
)
from routers.acls import get_photo

router = APIRouter()


@router.post("/locations", response_model=Union[LocationOut, Error])
def create_location(
    location: LocationIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: LocationRepository = Depends()
):
    try:
        photo_info = get_photo(location.name)
        picture_url = photo_info.get("picture_url", None)
    except Exception:
        return Error(message="Could not get photo")
    try:
        return repo.create(location, picture_url=picture_url)
    except Exception:
        return Error(message="Could not create location")


@router.get("/locations/{location_id}", response_model=Union[
    Optional[LocationOut],
    Error
])
def get_one(
    location_id: int,
    response: Response,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: LocationRepository = Depends(),
) -> LocationOut:
    try:
        location = repo.get_one(location_id)
        if location is None:
            response.status_code = 404
        return location
    except Exception:
        return Error(message="Could Not Get Location")


@router.get("/locations", response_model=Union[List[LocationOut], Error])
def get_all(
    repo: LocationRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        return repo.get_all()
    except Exception:
        return Error(message="Could Not Get Locations")


@router.put("/locations/{id}", response_model=Union[LocationOut, Error])
async def update_location(
        id: int,
        location: LocationUpdate,
        response: Response,
        repo: LocationRepository = Depends(),
        account_data: dict = Depends(authenticator.get_current_account_data)
) -> Union[LocationOut, Error]:
    try:
        update = repo.update_location(id, location)
        if type(update) is not LocationOut:
            response.status_code = 404
        return update
    except Exception:
        return Error(message="Could not update location")


@router.delete("/locations/{id}", response_model=bool)
def delete_location(
    id: int,
    repo: LocationRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    try:
        return repo.delete(id)
    except Exception:
        return Error(message="Could not Delete")
