from fastapi import (
    APIRouter,
    Depends,
    Response,
    HTTPException,
    status,
    Request,
)
from queries.accounts import (
    AdminIn,
    AdminRepository,
    AdminUpdate,
    AdminOut,
    Error,
)
from typing import Union, List
from authenticator import authenticator
from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


router = APIRouter()


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AdminOut


class HTTPError(BaseModel):
    detail: str


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AdminIn = Depends(authenticator.try_get_current_account_data)
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/admin", response_model=Union[AccountToken, Error])
async def create_admin(
    admin: AdminIn,
    request: Request,
    response: Response,
    repo: AdminRepository = Depends(),
):
    hashed_password = authenticator.hash_password(admin.password)
    try:
        account = repo.create(admin, hashed_password)
    except HTTPError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create account",
        )
    try:
        form = AccountForm(username=admin.username, password=admin.password)
        token = await authenticator.login(response, request, form, repo)
        return AccountToken(account=account, **token.dict())
        # return repo.create(admin)
    except Exception:
        return {"message": "Username already taken"}


@router.get("/admin", response_model=Union[Error, List[AdminOut], dict])
def get_all(repo: AdminRepository = Depends(),
            account_data: dict = Depends(
                authenticator.try_get_current_account_data
                ),
            ):
    if account_data:
        return repo.get_all()
    else:
        return {"message": "unauthorized"}


@router.put("/admin/update", response_model=Union[Error, AdminOut])
def update_admin(
    admin: AdminUpdate,
    repo: AdminRepository = Depends(),
    account_data: dict = Depends(
        authenticator.try_get_current_account_data
    ),
) -> Union[Error, AdminOut]:
    hashed_password = authenticator.hash_password(admin.new_password)
    if account_data:
        return repo.update(account_data, hashed_password)
    else:
        return {"message": "unauthorized"}


@router.delete("/admin/{admin_id}")
def delete_admin(
    admin_id: int,
    repo: AdminRepository = Depends(),
    account_data: dict = Depends(
        authenticator.try_get_current_account_data
    ),
):
    if account_data:
        return repo.delete(admin_id)
    else:
        return {"message": "unauthorized"}
