### 01/10/24

    - Created journal file

### 01/12/24

    - Redefined expectations/understandings surrounding goals for the project.
    Updated endpoints on that branch to align with project goals and modified
    wireframe to match the new endpoints.

### 01/16/24

    - Met with Paul to go over the design of our projects. Discussed idead for
    making the page more robust so that everyone has enough work to do, or
    making the design of the pages very visually appealing.

### 01/24/24

    - Today the group officially completed back end auth! Next goal is to work
    on login pages so that we can start on front end auth/protected endpoints,
    then the rest of our front end.

### 01/30/24

    - As of today we are almost fully done with front end authorization. I have
    started playing around with customizing some of the front end, but after
    lunch I will start on my unit testing for the listing and updating of
    accounts.

### 02/07/24

    - All backend endpoints are done, we are looking into adding a chat feature
    for employees as a stretch goal. Front end design is done and has been run
    by the team for approval. All that is really left now is to go through and
    make sure that all of our minimum requirements are done and to finish the
    README.

### 02/08/24
    - Plan for today is to do a super in-depth test of everything to double
    check that it works like it should. If not we will make one branch to
    mob code the issues on the spot.
