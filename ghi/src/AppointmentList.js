import { useEffect, useState } from "react";
import { NavLink, useNavigate } from 'react-router-dom';
import useToken from "@galvanize-inc/jwtdown-for-react";

export default function AppointmentList() {
  const { token } = useToken();
  const navigate = useNavigate();
  const [appointments, setAppointments] = useState([]);

  async function getAppointments(token) {
    const url = `${process.env.REACT_APP_API_HOST}/appointments`;
    if (token) {
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${token}` },
        method: "GET"
      });
      if (response.ok) {
        const data = await response.json();
        setAppointments(data);
      }
    }
  }

  useEffect(() => {
    getAppointments(token);
  }, [token])

  const handleDelete = async (e) => {
    e.preventDefault();
    const id = e.target.value;
    const url = `${process.env.REACT_APP_API_HOST}/appointments/${id}/`;
    await fetch(url, {
      headers: { Authorization: `Bearer ${token}` },
      method: "DELETE"
    });
    getAppointments(token);
  };

  const handleUpdate = async (e) => {
    const id = e.target.value
    navigate(`/appointments/update/${id}`)
  };

  if (token) {
    return (
      <>
        <div className="container-fluid text-center">
          <div className="card mb-3 p-3">
            <div className="row no-gutters">
              <div className="card-body ">
                <h1 className="mb-2">Appointments</h1>
                <p><NavLink to="/appointments/new" className="btn">Schedule New Appointment</NavLink></p>
              </div>
              <div className="row justify-content-center text-center">
                <div className='col'>
                  <table className='table table-striped table-hover'>
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Group Size</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Location</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {appointments.map(appointment => {
                        return (
                          <tr key={appointment.id}>
                            <td>{appointment.id}</td>
                            <td>{appointment.visitor_name}</td>
                            <td>{appointment.group_size}</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString(undefined, { hour: "numeric", minute: "numeric" })}</td>
                            <td>{appointment.location}</td>
                            <td>{appointment.phone_number}</td>
                            <td>{appointment.email}</td>
                            <td>
                              <button className="btn mx-2" onClick={handleUpdate} name='update' value={appointment.id}>Update</button>
                              <button className="btn mx-2" onClick={handleDelete} value={appointment.id} name='cancel'>Cancel</button>
                            </td>
                          </tr>
                        )
                      })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  } else {
    return (
      <div className="container-fluid text-center">
        <div className="card mb-3 p-3">
          <div className="row no-gutters">
            <div className="card-body ">
              <h1>Appointment List</h1>
              <p>You must be logged in to view this page.</p>
              <div className="button-spacing p-2">
                <NavLink to="/login" className="btn">Login</NavLink>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
