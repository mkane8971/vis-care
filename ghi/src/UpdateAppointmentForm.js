import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useParams, useNavigate, NavLink } from "react-router-dom";

function UpdateAppointmentForm() {
  const { appointment_id } = useParams();
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    visitor_name: "",
    date_time: "",
    group_size: "",
    phone_number: "",
    email: "",
    location: "",
  });

  const [locations, setLocations] = useState([]);

  const { token } = useToken();

  useEffect(() => {
    if (token) {
      const getData = async () => {
        try {
          const location_response = await fetch(`${process.env.REACT_APP_API_HOST}/locations`, {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`
          },
        });
        if (location_response.ok) {
          const location_data = await location_response.json();
          setLocations(location_data);
        } else {
          console.error("Failed to fetch locations");
        }
        const appointment_response = await fetch(`${process.env.REACT_APP_API_HOST}/appointments/${appointment_id}`, {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        });
        if (appointment_response.ok) {
          const appointment_data = await appointment_response.json();
          const adjustedDateTime = appointment_data.date_time ? new Date(appointment_data.date_time).toISOString().slice(0,16) : "";
          setFormData({
            ...appointment_data,
            date_time: adjustedDateTime,
          });
        } else {
          console.error("Failed to fetch appointment data");
        }
        } catch (error) {
          console.error("Error fetching data:", error);
        }
      };
      getData();
    }
  }, [token, appointment_id]);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleDateChange = (event) => {
    const localDate = new Date(event.target.value);
    const timezoneOffset = localDate.getTimezoneOffset() * 60000;
    const adjustedDate = new Date(localDate.getTime() - timezoneOffset);
    const formattedDate = adjustedDate.toISOString().slice(0, 16);
    setFormData((prevState) => ({
      ...prevState,
      date_time: formattedDate,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!token) {
      navigate("/login");
      return;
    }
    const updateAppointmentURL = `${process.env.REACT_APP_API_HOST}/appointments/${appointment_id}`;
    try {
      const response = await fetch(updateAppointmentURL, {
        method: "PUT",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.ok) {
        navigate("/appointments/");
      } else {
        console.error("Error submitting form:", response.statusText);
      }
    } catch (error) {
      console.error("Network error:", error);
    }
  };


  if (!token) {
    return (
      <div className="text-center">
        <h1>You must be logged in to update an appointment</h1>
        <NavLink className="btn" to="/login">
          Login
        </NavLink>
      </div>
    );
  }

  return (
<div className="card mb-3 p-3">
    <div className="row no-gutters">
        <div className="card-body ">
        <h1>Update Appointment</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                value={formData.visitor_name}
                onChange={handleChange}
                placeholder="First & Last Name"
                required
                type="text"
                name="visitor_name"
                id="visitor_name"
                className="form-control"
              />
              <label htmlFor="visitor_name"> Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.date_time}
                onChange={handleDateChange}
                placeholder="yyyy-MM-ddThh:mm"
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"
              />
              <label htmlFor="date_time"> Date & Time</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.group_size}
                onChange={handleChange}
                placeholder="##"
                required
                type="text"
                name="group_size"
                id="group_size"
                className="form-control"
              />
              <label htmlFor="group_size"> Group Size</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.phone_number}
                onChange={handleChange}
                placeholder="(xxx)xxx-xxxx"
                required
                type="text"
                name="phone_number"
                id="phone_number"
                className="form-control"
              />
              <label htmlFor="phone_number">
                {" "}
                Primary Visitor Phone Number
              </label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.email}
                onChange={handleChange}
                placeholder="example@visitor.com"
                required
                type="text"
                name="email"
                id="email"
                className="form-control"
              />
              <label htmlFor="email"> Primary Visitor E-mail Address</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={formData.location}
                onChange={handleChange}
                required
                name="location"
                id="location"
                className="form-control"
              >
                <option value="">Select Location</option>
                {locations.map((location) => (
                  <option key={location.id} value={location.id}>
                    {location.name}
                  </option>
                ))}
              </select>
              <label htmlFor="location"> Location</label>
            </div>
            <button className="btn">Update</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default UpdateAppointmentForm;
