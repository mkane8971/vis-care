import { BrowserRouter, Routes, Route } from 'react-router-dom';
import "./App.css";
import VisitForm from "./VisitForm.js";
import UpdateAppointmentForm from "./UpdateAppointmentForm.js";
import LocationForm from "./LocationForm";
import UpdateLocationForm from "./UpdateLocation.js";
import LocationList from "./LocationList.js";
import CreateAccount from "./CreateAccount.js";
import UpdateAccount from "./UpdateAccount.js";
import LoginForm from "./LoginForm.js";
import AdminList from "./AdminList.js";
import HomePage from "./HomePage.js";
import AppointmentList from './AppointmentList.js';
import Nav from "./Nav.js";
import Jammed from './Jammed.js';
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";


function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');
  const baseUrl = process.env.REACT_APP_API_HOST;

  return (
    <BrowserRouter basename={basename}>
      <AuthProvider baseUrl={baseUrl} >
        <Nav />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="login" element={<LoginForm />} />
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="update/:appointment_id" element={<UpdateAppointmentForm />} />
            <Route path="new" element={<VisitForm />} />
          </Route>
          <Route path="locations">
            <Route index element={<LocationList />} />
            <Route path="new" element={<LocationForm />} />
            <Route path="update/:id" element={<UpdateLocationForm />} />
          </Route>
          <Route path="accounts">
            <Route path="create" element={<CreateAccount />} />
            <Route path="update" element={<UpdateAccount />} />
            <Route path="list" element={<AdminList />} />
          </Route>
          <Route path="/*" element={<Jammed />} />
        </Routes>
      </AuthProvider >
    </BrowserRouter>
  );
}

export default App;
