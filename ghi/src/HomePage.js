import { NavLink } from 'react-router-dom';

function HomePage() {
    return (
        <div>
        <h5 className="card-title">Welcome to VisCare</h5>
            <div className="card mb-3 m-1 p-3">
                <div className="row no-gutters">
                    <div className="card-body ">
                        <div className="signin-message p-2">
                            <p>If you already have an admin account please log in</p>
                        </div>
                        <div className="button-spacing p-2">
                            <NavLink to="login" className="btn">Login</NavLink>
                        </div>
                        <center><hr style={{width:"75%", height:"2px"}}></hr></center>
                        <div className="signin-message p-2">
                            <p>If you need to create an admin account click the button below</p>
                        </div>
                        <div className="button-spacing p-2">
                            <NavLink to="accounts/create" className="btn" >Create</NavLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomePage;
