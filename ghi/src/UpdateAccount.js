import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { NavLink } from "react-router-dom";
import { useNavigate } from 'react-router-dom';

function UpdateAccount() {
    const navigate = useNavigate();
    const [new_password, setNewPassword] = useState('');
    const { token } = useToken();

    const handlePasswordChange = (event) => {
        const value = event.target.value;
        setNewPassword(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            new_password,
        };

        const adminUrl = `${process.env.REACT_APP_API_HOST}/admin/update`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
        };
        const response = await fetch(adminUrl, fetchConfig);
        if (response.ok) {
            setNewPassword('');
            navigate("/appointments");
        }
    }
    if (token) {
        return (
            <div className="card mb-3 p-3">
                <div className="row no-gutters">
                    <div className="card-body ">
                        <h1>Update Password</h1>
                        <form onSubmit={handleSubmit} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input onChange={handlePasswordChange} autoComplete="true" placeholder="new_password" required type="password" name="new_password" id="new_password" value={new_password} className="form-control" />
                                <label htmlFor="new_password">New Password:</label>
                            </div>
                            <button className="btn">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    } else {
        return (
            <div className="container-fluid text-center">
                <div className="card mb-3 p-3">
                    <div className="row no-gutters">
                        <div className="card-body ">
                            <h1>Update Password</h1>
                            <p>You must be logged in to view this page.</p>
                            <div className="button-spacing p-2">
                                <NavLink to="/login" className="btn">Login</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>)
    }
}
export default UpdateAccount;
