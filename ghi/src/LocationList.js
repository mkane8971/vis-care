import { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { NavLink, useNavigate } from 'react-router-dom';

function LocationList(props) {
  const navigate = useNavigate();
  const [locations, setLocations] = useState([]);
  const [view, setView] = useState("list");
  const { token } = useToken();
  const changeView = () => {
    if (view === "list") {
      setView("cards");
    } else {
      setView("list");
    }
  };
  const getData = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/locations`,
      {
        method: "GET",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response.ok) {
      const data = await response.json();
      setLocations(data);
    } else {
      alert("Did not get Data")
    }
  };
  const handleDelete = async (id) => {
    const url = `${process.env.REACT_APP_API_HOST}/locations/${id}/`;
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    const responseBody = await response.json();
    if (responseBody === true) {
      getData()
    } else {
      alert("Cannot delete a Location that has appointments")
    }
  };

  const handleUpdate = async (e) => {
    const id = e.target.value
    navigate(`/locations/update/${id}`)
  };


  useEffect(() => {
    if (token) {
      getData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);


  try {
    return (
      <div className="container-fluid text-center">
        {token ? (
          <>
            <div className="card mb-3 p-3">
              <div className="row no-gutters">
                <div className="list-card-body">
                  <h1 className="mb-2">Locations</h1>
                  <button className="btn m-2" onClick={changeView}>
                    {view === "cards"
                      ? "Switch to List View"
                      : "Switch to Card View"}
                  </button>
                  {view === "cards" ? (
                    locations.length > 0 ? (
                      <div className="row row-cols-7">
                        {locations.map((location) => (
                          <div className="col d-flex p-2" key={location.id}>
                            <div
                              className="card shadow"
                              style={{ width: "18rem" }}
                            >
                              <img
                                src={location.picture_url}
                                className="location-card-img-top"
                                alt={location.name}
                              />
                              <div className="card-body">
                                <h5 className="card-title">{location.name}</h5>
                                {location.num_appts < 3 ? (
                                  <p className="card-text">
                                    Id: {location.id},{" "} Number of appts:{" "}
                                    {location.num_appts},{" "} It is not busy.
                                  </p>
                                ) : (
                                  <p className="card-text">
                                    Id: {location.id},{" "} Number of appts:{" "}
                                    {location.num_appts}, It is busy.
                                  </p>
                                )}
                                <button
                                  className="btn mx-2"
                                  onClick={handleUpdate}
                                  name="update"
                                  value={location.id}
                                >
                                  Update
                                </button>
                                {location.num_appts === 0 ? (
                                  <button
                                    type="button"
                                    className="btn"
                                    onClick={() => handleDelete(location.id)}
                                  >
                                    Delete
                                  </button>
                                ) : (
                                  <>
                                    <p></p>
                                    <button
                                      type="button"
                                      className="!btn btn-danger btn-sm"
                                      disabled
                                      onClick={() => handleDelete(location.id)}
                                    >
                                      Cannot delete locations with appointments
                                    </button>
                                  </>
                                )}
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    ) : (
                      <p>No Locations available</p>
                    )
                  ) : locations.length > 0 ? (
                    <div>
                      <table className="table table-striped">
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Picture</th>
                            <th>Number of Appointments</th>
                            <th>Analysis</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {locations.map((location) => (
                            <tr key={location.id}>
                              <td>{location.id}</td>
                              <td>{location.name}</td>
                              <td>
                                <img
                                  src={location.picture_url}
                                  alt={location.name}
                                  style={{ height: 100, width: 100 }}
                                />
                              </td>
                              <td>{location.num_appts}</td>
                              {location.num_appts < 3 ? (
                                <td>
                                  Id: {location.id}, Number of appts:{" "}
                                  {location.num_appts}, It is not busy.
                                </td>
                              ) : (
                                <td>
                                  Id: {location.id}, Number of appts:{" "}
                                  {location.num_appts}, It is busy.
                                </td>
                              )}
                              <td>
                                <button
                                  className="btn mx-2"
                                  onClick={handleUpdate}
                                  name="update"
                                  value={location.id}
                                >
                                  Update
                                </button>
                                <button
                                  type="button"
                                  className="btn"
                                  onClick={() => handleDelete(location.id)}
                                >
                                  Delete
                                </button>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  ) : (
                    <div className="message">
                      <p>No Locations available</p>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </>
        ) : (
          <>
            <div className="card mb-3 p-3">
              <div className="row no-gutters">
                <div className="card-body ">
                  <h1>Location List</h1>
                  <p>You must be logged in to view this page.</p>
                  <div className="button-spacing p-2">
                    <NavLink to="/login" className="btn">Login</NavLink>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    );
  } catch (e) {
    console.log("Error", e);
  }
}

export default LocationList;
