import { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { NavLink } from 'react-router-dom';
import "./style.css";

function AdminList() {
    const { token } = useToken();
    const [admin, setAdmin] = useState([]);

    const getData = async () => {
        const url = `${process.env.REACT_APP_API_HOST}/admin`;
        const response = await fetch(url, {
            method: "get",
            body: JSON.stringify(getData),
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
        });
        if (response.ok) {
            const data = await response.json();
            setAdmin(data)
        }
    };

    // const getCurrentUser = async function () {
    //     const currentUser = await parse.User.current();
    //     if (currentUser != null) {
    //         Alert.alert(
    //             'Success!',
    //             `${currentUser.get('username')} is current user!`,
    //         );
    //     }
    //     return currentUser;
    // };

    useEffect(() => {
        getData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);



    // what is in the array causes use effect to fire. you log in token updates and token changes so it get the data
    return (
        <div className="container-fluid text-center">
            <div className="card mb-3 p-3">
                <div className="row no-gutters">
                    <div className="card-body" style={{ width: '300px' }}>
                        <h1>Admin List</h1>
                        {Array.isArray(admin) ?
                            <div className="content">
                                <table className="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">Usernames</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {admin.map(admin => {
                                            return (
                                                <tr key={admin.id}>
                                                    <td>{admin.username}</td>
                                                </tr>
                                            )
                                        })
                                        }
                                    </tbody>
                                </table>
                            </div>
                            :
                            <>
                                <p>You must be logged in to view this page.</p>
                                <div className="button-spacing p-2">
                                    <NavLink to="/login" className="btn">Login</NavLink>
                                </div>
                            </>
                        }
                    </div>
                </div>
            </div>
        </div >



    )
}

export default AdminList
