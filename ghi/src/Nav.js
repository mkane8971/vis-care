import { Link, useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

function Nav() {
    const { token, logout } = useToken();
    const navigate = useNavigate();
    const LogOutClick = () => {
        try {
            logout();
            navigate("/");
        } catch (e) {
            console.error(e);
        }
    };

    const isLoggedIn = Boolean(token);

    return (
      <nav className="mask">
        <h1>
          <Link className="project-name" to="/appointments">
            VisCare
          </Link>
        </h1>
        <ul className="list">
          <Link className="navbar-brand p-1" to="/accounts/list">
            Account List
          </Link>
          <Link className="navbar-brand p-1" to="/accounts/update">
            Update Account
          </Link>
          <Link className="navbar-brand p-1" to="/appointments/new">
            Schedule an Appointment
          </Link>
          <Link className="navbar-brand p-1" to="/appointments">
            Appointment List
          </Link>
          <Link className="navbar-brand p-1" to="locations">
            Locations
          </Link>
          <Link className="navbar-brand p-1" to="locations/new">
            Add a Location
          </Link>
          {isLoggedIn ? (
            <button className="btn" type="button" onClick={LogOutClick}>
              Logout
            </button>
          ) : (
            <Link className="btn" to="/login">
              Log In
            </Link>
          )}
        </ul>
      </nav>
    );
}

export default Nav;
