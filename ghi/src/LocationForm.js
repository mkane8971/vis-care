import useToken from "@galvanize-inc/jwtdown-for-react";
import React, { useState } from "react";
import { useNavigate, NavLink } from "react-router-dom";


function LocationForm() {
  const [formData, setFormData] = useState({
    name: "",
  });

  const { token } = useToken();
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = `${process.env.REACT_APP_API_HOST}/locations`;
    const response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(formData),
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }
    }
    );

    if (response.ok) {
      navigate("/locations")
    }
  };
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };
  if (token) {
    return (
      <div className="card mb-3 p-3">
        <div className="row no-gutters">
          <div className="card-body ">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="Name">Name</label>
              </div>
              <button className="btn">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <>
        <div className="card mb-3 p-3">
          <div className="row no-gutters">
            <div className="card-body ">
              <h1>Location Form</h1>
              <p>You must be logged in to view this page.</p>
              <div className="button-spacing p-2">
                <NavLink to="/login" className="btn">Login</NavLink>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default LocationForm;
