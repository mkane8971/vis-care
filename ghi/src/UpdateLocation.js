import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useParams, useNavigate } from "react-router-dom";

export default function UpdateLocationForm() {
  const { token } = useToken();
  const { id } = useParams();
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
  });

  async function getData(token) {
    if (token) {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/locations/${id}`,
        {
          headers: { Authorization: `Bearer ${token}` },
          method: "GET"
        });
      if (response.ok) {
        const data = await response.json();
        setFormData(data)
      }
    }
  };

  useEffect(() => {
    if (!(token)) {
      navigate("/");
    }
    getData(token);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token, navigate])

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = `${process.env.REACT_APP_API_HOST}/locations/${id}`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setFormData({
          name: '',
          picture_url: '',
        });
        navigate("/locations")
      } else {
        console.error("Error submitting form");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  return (
    <div className="card mb-3 p-3">
      <div className="row no-gutters">
        <div className="card-body ">
          <h1>Update Location</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                value={formData.name}
                onChange={handleFormChange}
                placeholder="Location Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="visitor_name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.picture_url}
                onChange={handleFormChange}
                placeholder="Picture URL"
                required
                type="url"
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="visitor_name">Picture URL</label>
            </div>
            <button className="btn">Update</button>
          </form>
        </div>
      </div>
    </div>
  );
}
