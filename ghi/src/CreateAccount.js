import { useState } from "react";
import { useNavigate } from 'react-router-dom';

function CreateAccount() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();

    const handleUsernameChange = (event) => {
        const value = event.target.value;
        setUsername(value);
    }

    const handlePasswordChange = (event) => {
        const value = event.target.value;
        setPassword(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            username,
            password,
        };


        const adminUrl = `${process.env.REACT_APP_API_HOST}/admin/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(adminUrl, fetchConfig);
        const updateResponse = await response.json();
        if (updateResponse.message === "Username already taken") {
            alert("Username is already taken, try again.")
        }
        else if (response.ok) {
            setUsername('');
            setPassword('');
            navigate("/login")
        };
    }

    return (
        <div className="card mb-3 p-3">
            <div className="row no-gutters">
                <div className="card-body ">
                    <h1>Create an account</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleUsernameChange} placeholder="username" required type="text" name="username" id="username" value={username} className="form-control" />
                            <label htmlFor="username">Username: </label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePasswordChange} placeholder="password" autoComplete="true" required type="password" name="password" id="password" value={password} className="form-control" />
                            <label htmlFor="password">Password:</label>
                        </div>
                        <button className="btn">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateAccount
