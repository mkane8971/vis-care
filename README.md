
# VisCare by The Jam
## Contributors
- Alex Lopez
- Joshua Covell
- Matthew Kane
- Tyler Henderson
## VisCare
- Visitor Management System

## Target Market:
Healthcare Facilities interested in a holistic building management policy. This technology can securely create appointments to track visitor flow, providing essential insights for strategic decision-making and operational efficiency.
## Local Machine Developer Project Initialization
1. Fork this repository
2. Clone the forked repository onto your local computer:
git clone <(https://gitlab.com/the-jam-mod3/vis-care.git)>
3. Build and run the project using Docker with these commands in your terminal:
```
docker volume create postgres-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all docker containers are running
- Once the React container has finished compiling (can take up to a few minutes), you can view the React Frontend project in your browser: http://localhost:3000/ ,or the
FastAPI Swagger UI backend at: http://localhost:8000/docs
## Deployed Project Access
1. To utilize deployed project visit https://the-jam-mod3.gitlab.io/vis-care/ on a web browser.
## Design
Vis care has three tables stored in a RDMS. Admins, Locations, and Appointments
Data for each of these tables can be created, read, updated, and deleted.
The queries folder contains query files to manage instances in the database.
The queries.accounts file will manage the changes in the admin table.
The queries.appointments file will manage the changes in the appointments table.
The queries.locations file will manage the changes in the locations table.
The routers folders contains router files that will manage the RESTful FastAPI backend points that will use a dependency injection
to access the repository of queries. Some of these endpoints will be protected to have a different functionality depending on whether a login token is available.
![VisCare Wireframe](/WireFrame.png)
## Backend RESTful Routes
These endpoints have a dependency on a function in a query repository corresponding with each table below to query the database and do not interact directly with the database.
## Accounts
| Operation            | URL Path              | Description                                     |
|----------------------|-----------------------|-------------------------------------------------|
|`GET`               | `/token`              | Retrieves FastAPI token from the browser cookies|
|`GET`, `POST`         | `/admin`              | Retrieves all accounts, Creates Admin account   |
|`PUT`, `DELETE`       | `/admin/<int:id>`     | Updates account, Deletes account instance       |

## Appointments
| Operation            | URL Path                | Description                                          |
|----------------------|-------------------------|------------------------------------------------------|
|`GET`, `POST`         | `/appointments`         |Retrieves all appointments, Creates Admin appointments|
|`GET`, `PUT`, `DELETE`| `/appointments/<int:id>`|Get One,Update, Delete an appoinment instance         |

## Locations
| Operation            | URL Path              | Description                                       |
|----------------------|-----------------------|---------------------------------------------------|
|`GET`, `POST`         | `/locations`          |Retrieves all locations, Creates location instance |
|`GET`, `PUT`, `DELETE`| `/locations/<int:id>` |Get One location,Updates location, Deletes location|
## 3rd Party Pexels API
| Operation                       | URL Path                         | Description                                        |
|---------------------------------|----------------------------------|----------------------------------------------------|
|`query`: hospital <location.name>|"https://api.pexels.com/v1/search"|Retrieves a photo from pexels on `/locations` `POST`|
## Front-end deployed URL routes
-All paths will begin with `https://the-jam-mod3.gitlab.io/vis-care/` followed by the path below to
arrive at the page.
| URL Path             | Description                                                                  |
|----------------------|------------------------------------------------------------------------------|
|` `                   | Index page that has buttons to direct to other pages                         |
|`login`               | Allows a user with an account to enter detail to acquire token in the cookies|
|`accounts/create`     | Allows a user to enter account info to send to database                      |
|`accounts/list`       | Lists all accounts of users in the database                                  |
|`accounts/update`     | Allows users to update their password to login                               |
|`appointments`        | Lists all appointments that are in the database                              |
|`appointments/new`    | Allows users to creat a new instance of an appointment                       |
|`appointments/update` | Allows users to update an appointment instance                               |
|`locations`           | Allows users to view all locations in the database                           |
|`locations/new`       | Allows users to create a new location instance                               |
|`locations/update/:id`| Allows users to update an instance of a location                             |
## API EndPoint expectations
### <u> Create Admin </u>
**Endpoint path**: `/admin`
**Endpoint method**: `POST`
**Headers**:
- `Authorization`: Bearer token
- Request body:
  ```json
  {
    "admin": [
      {
          "username": string,
          "password": string,
      }
    ]
  }
  ```
---
### <u> Admin List </u>
**Endpoint path**: `/admin`
**Endpoint method**: `GET`
**Headers**:
- `Authorization`: Bearer token
- Response: A list of admins
- Response shape:
  ```json
  {
    "admin": [
      {
          "id": int,
          "username": string
      }
    ]
  }
  ```
---
### <u> Update Admin </u>
**Endpoint path**: `/admin/<int:id>
`
**Endpoint method**: `PUT`
**Headers**:
- `Authorization`: Bearer token
- Request shape (JSON):
  ```json
  {
    "admin": [
      {
        "username": string,
        "password": string
      }
    ]
  }
  ```
- Response: Upon creation of admin account they are redirected to the appointment list page
- Response shape (JSON):
  ```json
  {
    "admin": [
      {
          "id": int,
          "username": string,
      }
    ]
  }
  ```
---
###  <u> Delete admin </u>
**Endpoint path**: `/admin/<int:id>`
**Endpoint method**: `DELETE`
-**Headers**:
- `Authorization`: Bearer token
- Response: Potential pop up notification for deletion of account after redirect
- Response shape (JSON):
  ```json
  "Admin successfully deleted."
  ```
---
### <u> Create Appointment </u>
**Endpoint path**: `/appointments`
**Endpoint method**: `POST`
**Headers**:
- `Authorization`: Bearer token
- Request body:
  ```json
  {
    "appointments": [
      {
          "visitor_name": string,
          "group_size": int,
          "date_time": datetime,
          "location": string,
          "phone_number": string,
          "email": string
      }
    ]
  }
  ```
- Response: Creation of new appointment
- Response shape:
  ```json
  {
    "appointments": [
      {
          "id": int,
          "visitor_name": string,
          "group_size": int,
          "date_time": datetime,
          "location": string,
          "phone_number": string,
          "email": string
      }
    ]
  }
  ```
---
### <u> Appointment List </u>
**Endpoint path**: `/appointments`
**Endpoint method**: `GET`
**Headers**:
- `Authorization`: Bearer token
- Response: A list of appointments
- Response shape:
  ```json
  {
    "appointments": [
      {
        "id": int,
        "visitor_name": string,
        "group_size": int,
        "date_time": datetime,
        "status": Pending/Approved/Rejected,
        "location": string,
        "phone_number": int,
        "email": string
      }
    ]
  }
  ```
---
### <u> Update Appointment </u>
**Endpoint path**: `/appointment/<int:id>`
**Endpoint method**: `PUT`
**Headers**:
- `Authorization`: Bearer token
- Request shape (JSON):
  ```json
  {
    "appointments": [
      {
        "visitor_name": string,
        "group_size": int,
        "date_time": datetime,
        "location": string,
        "phone_number": int,
        "email": string
      }
    ]
  }
  ```
- Response: Return all appointment details including what was updated
- Response shape (JSON):
  ```json
  {
    "appointments": [
      {
          "visitor_name": string,
          "group_size": int,
          "date_time": datetime,
          "location": string,
          "phone_number": int,
          "email": string
      }
    ]
  }
  ```
---
### <u> Delete Appointment </u>
**Endpoint path**: `/appointment/<int:id>`
**Endpoint method**: `DELETE`
**Headers**:
- `Authorization`: Bearer token
- Response: Message relaying deletion of appointment
- Response shape (JSON):
  ```json
  "Appointment successfully deleted."
  ```
---
### <u> Create locations </u>
**Endpoint path**: `/locations`
**Endpoint method**: `POST`
-**Headers**:
- `Authorization`: Bearer token
- Request body:
  ```json
  {
    "locations": [
      {
          "name": string
      }
    ]
  }
  ```
- Response: Creation of new locations
- Response shape:
  ```json
  {
    "locations": [
      {
          "id": int,
          "name": string,
          "picture_url": string |null
      }
    ]
  }
  ```
---
### <u> Location List </u>
**Endpoint path**: `/locations`
**Endpoint method**: `GET`
**Headers**:
- `Authorization`: Bearer token
- Response: A list of locations in the hospital
- Response shape:
  ```json
  {
    "locations": [
      {
        "id": int,
        "name": string,
        "picture_url": string | null
      }
    ]
  }
  ```
---
### <u> Location View </u>
**Endpoint path**: `/locations/<int:id>`
**Endpoint method**: `GET`
**Headers**:
- `Authorization`: Bearer token
- Response: Details of one location in the hospital
- Response shape:
  ```json
  {
    "locations": [
      {
        "id": int,
        "name": string,
        "picture_url": string | null
      }
    ]
  }
  ```
---
### <u> Update locations </u>
**Endpoint path**: `/locations/<int:id>`
**Endpoint method**: `PUT`
**Headers**:
- `Authorization`: Bearer token
- Request shape (JSON):
  ```json
  {
    "locations": [
      {
          "name": string,
          "picture_url": string
      }
    ]
  }
  ```
- Response: Return all location details including what was updated
- Response shape (JSON):
  ```json
  {
    "locations": [
      {
          "name": string,
          "picture_url": string | null
      }
    ]
  }
  ```
---
### <u> Delete location </u>
**Endpoint path**: `/locations/<int:id>`
**Endpoint method**: `DELETE`
**Headers**:
- `Authorization`: Bearer token
- Response: Alert for deletion of account after redirect to list of locations
- Response shape (JSON):
  ```json
  "Locations successfully deleted."
  ```
